#include "mode1.h"


#define	CDC_NONE			0
#define CDC_CHECKDISK		(CDC_NONE+1)
#define CDC_PLAY			(CDC_CHECKDISK+1)
#define CDC_PLAY_ONE		(CDC_PLAY+1)
#define CDC_STOP			(CDC_PLAY_ONE+1)
#define CDC_TRACKINFO		(CDC_STOP+1)
#define CDC_PLAY_LOOP		(CDC_TRACKINFO+1)
#define CDC_PAUSETOGGLE		(CDC_PLAY_LOOP+1)

static u8 done;
static u8 first_track = 0, last_track = 0, curr_track = 0;
static u8 intChar[9]; //8char +0
static u8 cdcommand;



static void check_disc()
{
	MackMain_checkDisc( );
}

static void play_from()
{
	MackMain_CDDA_play(curr_track, PLAY_MODE_ONCE);
} 

/*
 * Play Track Once
 */
static void play_track()
{
	MackMain_CDDA_play(curr_track, PLAY_MODE_TRACK);
} 

/*
 * Stop Playback
 */
static void stop_cd()
{
	MackMain_CDDA_stop();
} 

/*
 * Get Track Info
 */
 static void get_track_info()
 {
	u32 time_tno = 0;
    u8 type;

	if (!MackMain_getTrackInfo(curr_track, &time_tno, &type))	return;

	//TODO : hexa not dec
	//sprintf(text, "%08X", time_tno);
	VDP_drawText("Track Time:         ", 2, 13);
	intToStr(time_tno,intChar,8);
	VDP_drawText(intChar, 14, 13);
	
	VDP_drawText("Track Type:     ", 2, 14);
	VDP_drawText( ((type)? "DATA" : "CDDA"), 14, 14);
}

/*
 * Play Track and Repeat
 */
static void play_repeat()
{
	MackMain_CDDA_play(curr_track, PLAY_MODE_TRACK);
}

/*
 * Pause/Resume
 */
static void pause_resume()
{
	MackMain_CDDA_pause();
}


/*
 * Get Disc Info
 */
 static void get_disc_info()
 {
	discinfo info;
	
	if ( !MackMain_getDiscInfo(&info) )	return;


	VDP_drawText("Status:", 2, 4);
	if (info.biosStatus & STATUS_NOT_READY)
	{
		VDP_drawText("NOT READY           ", 10, 4);
	}
	else
	{
		switch (info.biosStatus)
		{
			case STATUS_STOPPED:
				VDP_drawText("STOPPED             ", 10, 4);
				break;
			case STATUS_PLAYING:
				VDP_drawText("PLAYING             ", 10, 4);
				break;
			case STATUS_SCANNING:
				VDP_drawText("SCANNING            ", 10, 4);
				break;
			case STATUS_PAUSED:
				VDP_drawText("PAUSED              ", 10, 4);
				break;
			case STATUS_SEEKING:
				VDP_drawText("SEEKING             ", 10, 4);
				break;
			case STATUS_NODISC:
				VDP_drawText("NO DISC             ", 10, 4);
				break;
			case STATUS_READING_TOC:
				VDP_drawText("READING TOC         ", 10, 4);
				break;
			case STATUS_OPEN:
				VDP_drawText("OPEN                ", 10, 4);
				break;
			default:
				VDP_drawText("UNDEFINED           ", 10, 4);
				break;
		}
	}

	first_track = info.cdd_status.firstTrackIdx;
	last_track = info.cdd_status.lastTrackIdx;
	if (curr_track < first_track)
		curr_track = first_track;
	if (curr_track > last_track)
		curr_track = last_track;

	VDP_drawText("First Track:    ", 2, 6);
	intToStr(first_track,intChar,2);
	VDP_drawText(intChar, 15, 6);

	VDP_drawText("Last Track:    ", 2, 7);
	intToStr(last_track,intChar,2);
	VDP_drawText(intChar, 14, 7);

	VDP_drawText("Current Track:    ", 2, 8);
	intToStr(curr_track,intChar,2);
	VDP_drawText(intChar, 17, 8);

	VDP_drawText((info.cdd_status.flags & FLAG_DATA) ? "DATA" : "CDDA", 2, 10);
	VDP_drawText((info.cdd_status.flags & FLAG_EMP)  ? "EMP" : "LIN", 7, 10);
	VDP_drawText((info.cdd_status.flags & FLAG_MUTE) ? "MUTED" : "     ", 11, 10);

	VDP_drawText("Drive Version:    ", 2, 11);
	intToStr(info.cdd_status.driveVersion,intChar,2);
	VDP_drawText(intChar, 17, 11);
}


static void handleJoy(u16 joy, u16 key, u16 newState)
{
//	done = (newState & BUTTON_ALL);

	//I use == to avoid multi buttons press
	if (newState == BUTTON_START)
	{
		cdcommand = CDC_CHECKDISK;
	}
	else if (newState == BUTTON_A)
	{
		cdcommand = CDC_PLAY;
	}
	else if (newState == BUTTON_B)
	{
		cdcommand = CDC_PLAY_ONE;
	}
	else if (newState == BUTTON_C)
	{
		cdcommand = CDC_STOP;
	}
	else if (newState == BUTTON_X)
	{
		cdcommand = CDC_TRACKINFO;
	}
	else if (newState == BUTTON_Y)
	{
		cdcommand = CDC_PLAY_LOOP;
	}
	else if (newState == BUTTON_Z)
	{
		cdcommand = CDC_PAUSETOGGLE;
	}
	else if ( newState & (BUTTON_DOWN | BUTTON_RIGHT))
	{
		curr_track++;
		if (curr_track > last_track)
			curr_track = first_track;
	}
	else if ( newState & (BUTTON_UP | BUTTON_LEFT))
	{
		curr_track--;
		if (curr_track < first_track)
			curr_track = last_track;
	}

}

static void jumpNext()
{
//	setCurrentState(XXXXX_STATE);
}

static void cdplayerInit( )
{
	done = FALSE;

	VDP_drawText("Mode 1 CD Player", 20-8, 2);
	VDP_drawText("DPAD = Change Track START = Check Disc", 2, 23);
	VDP_drawText("A = Play On   B = Play Once   C = Stop", 2, 24);
	VDP_drawText("X = Trk Info Y = Play Repeat Z = Pause", 2, 25);

	MickInput_setPressedCallback(&handleJoy);
}

static void cdplayerUpdate( )
{
 	//press button to pass
	if (done)
	{
		jumpNext();
		return;
	}

	switch(cdcommand)
	{
		case CDC_CHECKDISK:
			check_disc();
			break;
	
		case CDC_PLAY:
			play_from();
			break;
		
		case CDC_PLAY_ONE:
			play_track();
			break;
		
		case CDC_STOP:
			stop_cd();
			break;
	
		case CDC_TRACKINFO:
			get_track_info();
			break;
	
		case CDC_PLAY_LOOP:
			play_repeat();
			break;
	
		case CDC_PAUSETOGGLE:
			pause_resume();
			break;

		case CDC_NONE:
		default:
			get_disc_info();
			break;
	}


	//this will make us sure get_disc_info is every frame, unless button press of course
	cdcommand = CDC_NONE;
}

MickState cdplayerState =
{
		CDPLAYER_STATE,
		&cdplayerInit,
		&cdplayerUpdate,
		NULL, //VUpdate
		NULL, //HUpdate
		NULL  //close
};
