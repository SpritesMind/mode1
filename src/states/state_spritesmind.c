#include "mode1.h"

#define	PAL_LOGO	PAL0
#define	FADE_STEPS	25

#define DELAY	200 //*3
static int delayCounter;
static u8 done;

static void handleJoy(u16 joy, u16 key, u16 newState)
{
	done = (newState & BUTTON_ALL);
}

static void jumpNext()
{
	setCurrentState(CDPLAYER_STATE);
}

static void logoInit( )
{
	done = FALSE;

	VDP_setPalette(PAL_LOGO, palette_black);
	
	unpackRLE( spritesmind.tiles, GFX_WRITE_VRAM_ADDR(TILE_USER), spritesmind.compressedSize);
	VDP_fillTileMapRectInc(BPLAN, TILE_ATTR_FULL(PAL_LOGO, 0, 0, 0, TILE_USERINDEX), 7, 8, spritesmind.width, spritesmind.height);

	delayCounter = 0;
	VDP_fadePal(PAL_LOGO, palette_black, spritesmind.pal, FADE_STEPS, TRUE);

	MickInput_setPressedCallback(&handleJoy);
}

static void logoUpdate( )
{
	//press button to pass
	if (done)
	{
		jumpNext();
		return;
	}

	delayCounter++;
	//..or auto pass after give delay
	if ( delayCounter == DELAY )
	{
		VDP_fadePal(PAL_LOGO, spritesmind.pal, palette_black, FADE_STEPS, TRUE);
	}
	else if ( delayCounter == (DELAY+FADE_STEPS) )
	{
		jumpNext();
		return;
	}

	//nothing
}

MickState logoState =
{
		LOGO_STATE,
		&logoInit,
		NULL,
		&logoUpdate,
		NULL,
		NULL
};
