#include "mode1.h"

void setCurrentState(int stateID)
{
	VDP_setEnable(FALSE);

	/**
	 * 
	 * lighter than VDP_init and reset only what YOU really need
	 * 
	 **/
	VDP_resetSprites();
	VDP_updateSprites();
		
	//WARNING : this way, it disables it on every state change
	//VDP_setHInterrupt(FALSE);
	
	VDP_clearPlan(APLAN, 1);
    VDP_waitDMACompletion();
    VDP_clearPlan(BPLAN, 1);
    VDP_waitDMACompletion();
	
	VDP_setEnable(TRUE);

	
	MickInput_init(1);
		
    // load defaults palettes
    VDP_setPalette(PAL0, palette_grey);
    VDP_setPalette(PAL1, palette_red);
    VDP_setPalette(PAL2, palette_green);
    VDP_setPalette(PAL3, palette_blue);
	
	switch(stateID)
	{
		case LOGO_STATE:
			MickState_setCurrent( &logoState );
			break;
		case CDPLAYER_STATE:
			MickState_setCurrent( &cdplayerState );
			break;
		/*
		case DEBUG_STATE:
			MickState_setCurrent( &debugState );
			break;
		*/
	}
}
