#ifndef STATEMANAGER_H_
#define STATEMANAGER_H_

#include <mick.h>

#define LOGO_STATE		1
#define CDPLAYER_STATE	(LOGO_STATE + 1)
//#define DEBUG_STATE 99

extern MickState logoState;
extern MickState cdplayerState;
/*
extern MickState debugState;
*/

void setCurrentState(int stateID);

#endif /* STATEMANAGER_H_ */
