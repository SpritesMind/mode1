#include "mode1.h"


static u8 mcdIsAvaible = FALSE;


void VBlank_sub( )
{
	MackMain_updateVBlank();
	MickState_updateVBlank();
}
/*
static void HBlank_sub( )
{
	MickState_updateHBlank();
}
*/

int main()
{
	//could be set anytime BEFORE first MackMain_waitSub
	SYS_setVIntCallback( &VBlank_sub );
	// not needed here
	//SYS_setHIntCallback( &MickState_updateHBlank() ); //&HBlank_sub );

	mcdIsAvaible = FALSE;

	//load sub bios
	if ( MackMain_boot(SYSTEM_CART_W_CD) == BOOT_IN_PROGRESS )
	{
		u8 bootStatus = BOOT_IN_PROGRESS;
		while ( bootStatus == BOOT_IN_PROGRESS)
		{
			bootStatus = MackMain_isBooting();
			//TODO : animate stuff ;)
			VDP_waitVSync();
		}
		
		mcdIsAvaible = (bootStatus == BOOT_DONE);
	}
	
	setCurrentState( LOGO_STATE );

    while(1)
    {	
    	VDP_waitVSync();
    	MickState_update();
    }

	return 0;
}
