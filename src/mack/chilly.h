#ifndef CHILLY_H_
#define CHILLY_H_

// Chilly Stuff


extern void syncVInt();

extern short set_sr(short new_sr);
extern void write_byte(unsigned int dst, unsigned char val);
extern void write_word(unsigned int dst, unsigned short val);
extern void write_long(unsigned int dst, unsigned int val);
extern unsigned char read_byte(unsigned int src);
extern unsigned short read_word(unsigned int src);
extern unsigned int read_long(unsigned int src);

s8 memcmp(u8 *src, const u8 *from, u16 len);

#endif //CHILLY_H_
