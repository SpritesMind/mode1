#include "mack.h"


#define COMMAND_ACK		0x0
#define COMMAND_INITIALIZED	'I'	//from sub only
#define COMMAND_CHECKDISK	'C'
#define	COMMAND_DISKINFO	'D' // GetDiscInfo command
#define COMMAND_TRACKINFO	'T' // GetTrackInfo command
#define COMMAND_CDDA_STOP	'S'	// StopPlaying command
#define COMMAND_CDDA_PAUSE	'Z'	// PauseResume command
#define COMMAND_CDDA_PLAY	'P'	// PlayTrack command


#define GATE_ARRAY(x)	(0xA12000|(x))
#define MAIN_COMM_PORT	GATE_ARRAY(0x0E)
#define SUB_COMM_PORT	GATE_ARRAY(0x0F)


extern u32 Sub_Start;
extern u32 Sub_End;

extern void Kos_Decomp(u8 *src, u8 *dst);

static u8 systemMode = SYSTEM_CART;
static u8 biosType;
static u8 subAvailable = FALSE;
static u8 cartAvailable = FALSE;
static int bootTimeout= 0;
static u8 bootStep; 

static inline u8 readCommPort()
{
	return read_byte(SUB_COMM_PORT);
}

static inline void writeCommPort(u8 value)
{
	write_byte(MAIN_COMM_PORT, value);
}

static inline u8 isSubBusy()
{
	return (readCommPort() != COMMAND_ACK);
}


static u8 wait_cmd_ack(void)
{
    u8 ack = COMMAND_ACK;

    while (ack == COMMAND_ACK)
        ack = readCommPort(); // wait for acknowledge byte in sub comm port

    return ack;
}

static void wait_do_cmd(u8 cmd)
{
    while ( isSubBusy( ) ) ; // wait until Sub-CPU is ready to receive command
    writeCommPort(cmd); // set main comm port to command
}


void MackMain_updateVBlank()
{
    /*
     * Set the vertical blank handler to generate Sub-CPU level 2 ints.
     * The Sub-CPU BIOS needs these in order to run.
     */
	if (subAvailable)	syncVInt();

}

static u8 initMode1CD()
{
	u8 *bios;
		
	biosType = 0;
	
	/*
     * Check for CD BIOS
     * When a cart is inserted in the MD, the CD hardware is mapped to
     * 0x400000 instead of 0x000000. So the BIOS ROM is at 0x400000, the
     * Program RAM bank is at 0x420000, and the Word RAM is at 0x600000.
     */
    bios = (u8 *)0x415800;
    if (memcmp(bios + 0x6D, "SEGA", 4))
    {
		biosType = 2;
		            
        bios = (u8 *)0x416000;
        if (memcmp(bios + 0x6D, "SEGA", 4))
        {
			biosType = 3;

            // check for WonderMega/X'Eye
            if (memcmp(bios + 0x6D, "WONDER", 6))
            {
				biosType = 4;

                bios = (u8 *)0x41AD00; // might also be 0x40D500
                // check for LaserActive
                if (memcmp(bios + 0x6D, "SEGA", 4))
                {
					biosType = 0xFF;
#if defined(DEBUG)
                    KDebug_Alert("No CD detected!");
#endif
					return FALSE;
                }
            }
        }
    }
 
	/*
	 * Reset the Gate Array - this specific sequence of writes is recognized by
	 * the gate array as a reset sequence, clearing the entire internal state -
	 * this is needed for the LaserActive
	 */
	write_word(GATE_ARRAY(2), 0xFF00);
	write_byte(GATE_ARRAY(1), 0x03);
	write_byte(GATE_ARRAY(1), 0x02);
	write_byte(GATE_ARRAY(1), 0x00);

    /*
     * Reset the Sub-CPU, request the bus
     */
    write_byte(GATE_ARRAY(1), 0x02);
    while (!(read_byte(GATE_ARRAY(1)) & 2)) write_byte(GATE_ARRAY(1), 0x02); // wait on bus acknowledge

	
    /*
     * Decompress Sub-CPU BIOS to Program RAM at 0x00000
     */
	write_word(GATE_ARRAY(2), 0x0002); // no write-protection, bank 0, 2M mode, Word RAM assigned to Sub-CPU
//	memset((u8 *)0x420000, 0, 0x20000); // clear program ram first bank - needed for the LaserActive
	memset((u8 *)0x420000, 0, 0xFFFF); // clear program ram first bank - needed for the LaserActive
	memset((u8 *)0x430000, 0, 0xFFFF); // clear program ram first bank - needed for the LaserActive
	memset((u8 *)0x440000, 0, 0xFFFF); // clear program ram first bank - needed for the LaserActive
     
    Kos_Decomp(bios, (u8 *)0x420000);

	
    /*
     * Copy Sub-CPU program to Program RAM at 0x06000
     */
#if defined(DEBUG)
	KDebug_Alert("Copying Sub-CPU Program");
#endif
    memcpy((void *)0x426000, &Sub_Start, (u16) ((int)&Sub_End - (int)&Sub_Start));
    if (memcmp((void *)0x426000, (const u8 *) &Sub_Start, (u16) ((int)&Sub_End - (int)&Sub_Start)))
    {
#if defined(DEBUG)
		KDebug_Alert("Failed writing Program RAM!");
#endif
		return FALSE;
    }

    writeCommPort(0x00); // clear main comm port
    write_byte(GATE_ARRAY(2), 0x2A); // write-protect up to 0x05400
    write_byte(GATE_ARRAY(1), 0x01); // clear bus request, deassert reset - allow CD Sub-CPU to run
    while (!(read_byte(GATE_ARRAY(1)) & 1)) write_byte(GATE_ARRAY(1), 0x01); // wait on Sub-CPU running
#if defined(DEBUG)
		KDebug_Alert("Sub-CPU started");
#endif
    
    return TRUE;
}



static u8 isSubInitialized()
{
	if (!subAvailable)	return FALSE;

    /*
     * Wait for Sub-CPU program to set sub comm port indicating it is running -
     */
    return (readCommPort() == COMMAND_INITIALIZED);
 }


u8 MackMain_boot( u8 sysMode )
{	
	bootTimeout = 0;
	bootStep = 0;
	cartAvailable = FALSE;
	subAvailable = FALSE;
	
	
	systemMode = sysMode;
	switch( systemMode)
	{
		case SYSTEM_MODE0:
#if defined(DEBUG)
		KDebug_Alert("BIOS boot mode not supported");
#endif
			return BOOT_ERROR;
			break;
		case SYSTEM_CART:
		//case SYSTEM_MODE1_TYPE1:
			cartAvailable = TRUE;
			subAvailable = FALSE;
			return BOOT_DONE;
			break;
		case SYSTEM_CART_W_CD:
		//case SYSTEM_MODE1_TYPE2:
			cartAvailable = TRUE;
			subAvailable = initMode1CD( );
			if (!subAvailable)	return BOOT_ERROR;
			
			return BOOT_IN_PROGRESS;
			break;
		case SYSTEM_CD:
		//case SYSTEM_MODE2_TYPE1:
			//TODO
			// cartAvailable = FALSE;
			//	subAvailable = TRUE;
		case SYSTEM_CD_W_CART:
		//case SYSTEM_MODE2_TYPE2:
			//TODO
			// cartAvailable = TRUE;
			//	subAvailable = TRUE;
		default:
			break;
	}			
	
	return BOOT_ERROR;
}

u8 MackMain_isBooting( )
{
	if (!subAvailable)	return BOOT_ERROR;
	
	if ( (bootStep == 0) && !isSubInitialized() )
	{
		/*
		 * unless there's something wrong with the hardware, a timeout isn't needed...
		 * just loop until the Sub-CPU program responds, but 2000000 is about
		 * ten times what the LaserActive needs, and the LA is the slowest unit to
		 * initialize
		 */
		bootTimeout++;
		
		if (bootTimeout > 2000000)
		{
#if defined(DEBUG)
			KDebug_Alert("No life sign from Sub CPU");
#endif
			return BOOT_ERROR;
		}
		
		bootStep=1;
		return BOOT_IN_PROGRESS;
	}

 
	//wait for Sub-CPU to indicate it is ready to receive commands
	if ( (bootStep == 1) & isSubBusy())		return BOOT_IN_PROGRESS;
				
	bootStep=2;
#if defined(DEBUG)
	KDebug_Alert("Sub CPU ready for command");
#endif

	return BOOT_DONE;
}


/*
 * Check Disc
 */
void MackMain_checkDisc()
{
	u8 ack;
	if (!subAvailable)	return; // FALSE;

	wait_do_cmd(COMMAND_CHECKDISK); // CheckDisc command
	ack = wait_cmd_ack();
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result 	
}


/*
 * Play from Track to End
 */
void MackMain_CDDA_play( u8 track, u8 mode )
{
	u8 ack;
	if (!subAvailable)	return; // FALSE;

	write_word(GATE_ARRAY(0x10), track);
	write_byte(GATE_ARRAY(0x12), 0xFF); // play from track to end
	wait_do_cmd(COMMAND_CDDA_PLAY); // PlayTrack command
	ack = wait_cmd_ack();
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result
}

void MackMain_CDDA_pause( )
{
	u8 ack;
	if (!subAvailable)	return; // FALSE;

	wait_do_cmd(COMMAND_CDDA_PAUSE); // PauseResume command
	ack = wait_cmd_ack();
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result	
}


void MackMain_CDDA_stop( )
{
	u8 ack;
	if (!subAvailable)	return; // FALSE;

	wait_do_cmd(COMMAND_CDDA_STOP); // StopPlaying command
	ack = wait_cmd_ack();
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result
}
u8 MackMain_getTrackInfo(u8 track, u32 *trackTime, u8 *trackType)
{
	u8 ack;
	if (!subAvailable)	return FALSE;
	
	write_word(GATE_ARRAY(0x10), track);
	wait_do_cmd(COMMAND_TRACKINFO); // GetTrackInfo command
	ack = wait_cmd_ack();
	*trackTime = read_long(GATE_ARRAY(0x20)); // track time: MMSSFFTN
	*trackType = read_byte(GATE_ARRAY(0x24)); // track type: 00 = CDDA, FF = DATA
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result
	
	return TRUE;
}

u8 MackMain_getDiscInfo(discinfo *info)
{
	u8 ack;
	
	if (!subAvailable)	return FALSE;

	wait_do_cmd(COMMAND_DISKINFO); // GetDiscInfo command
	ack = wait_cmd_ack();
	if (ack != COMMAND_DISKINFO)
	{
		writeCommPort(COMMAND_ACK); // acknowledge receipt of command result
		return FALSE;
	}
	
	info->biosStatus = read_word(GATE_ARRAY(0x20));

	//TODO : others stuff
	

	info->cdd_status.firstTrackIdx = read_byte(GATE_ARRAY(0x20+2));
	info->cdd_status.lastTrackIdx = read_byte(GATE_ARRAY(0x20+3));
	//Chilly does this, but for my tests, it seems wrong...
	//perhaps it wrongly assume it was BCD a number...
	//TODO: ask him
	//info->cdd_status.firstTrackIdx = (info.cdd_status.firstTrackIdx & 0xF) + ((info.cdd_status.firstTrackIdx >> 4) * 10);
	//info->cdd_status.lastTrackIdx = (info.cdd_status.lastTrackIdx & 0xF) + ((info.cdd_status.lastTrackIdx >> 4) * 10);
	info->cdd_status.driveVersion = read_byte(GATE_ARRAY(0x20+4));
	info->cdd_status.flags = read_byte(GATE_ARRAY(0x20+5));
	
	writeCommPort(COMMAND_ACK); // acknowledge receipt of command result
	
	return TRUE;
}
