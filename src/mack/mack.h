#ifndef MACK_H_
#define MACK_H_

#include <genesis.h>
#include <genres.h>

#include "chilly.h"


#include "mack_common.h"
#include "mack_main.h"
#include "mack_sub.h"

#endif /* MACK_H_ */
