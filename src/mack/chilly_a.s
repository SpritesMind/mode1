
/** set CD VInt            **/
/** from Chilly Willy code **/
        .global syncVInt
syncVInt:
        movem.l %d0/%a0,-(%sp)
        lea     0xA12000,%a0
        move.w  (%a0),%d0
        ori.w   #0x0100,%d0
        move.w  %d0,(%a0)
        movem.l (%sp)+,%d0/%a0
*        rte
		rts

* void write_byte(void *dst, unsigned char val)
        .global write_byte
write_byte:
        movea.l 4(%sp),%a0
        move.l  8(%sp),%d0
        move.b  %d0,(%a0)
        rts

* void write_word(void *dst, unsigned short val)
        .global write_word
write_word:
        movea.l 4(%sp),%a0
        move.l  8(%sp),%d0
        move.w  %d0,(%a0)
        rts

* void write_long(void *dst, unsigned int val)
        .global write_long
write_long:
        movea.l 4(%sp),%a0
        move.l  8(%sp),%d0
        move.l  %d0,(%a0)
        rts

* unsigned char read_byte(void *src)
        .global read_byte
read_byte:
        movea.l 4(%sp),%a0
        move.b  (%a0),%d0
        rts

* unsigned short read_word(void *src)
        .global read_word
read_word:
        movea.l 4(%sp),%a0
        move.w  (%a0),%d0
        rts

* unsigned int read_long(void *src)
        .global read_long
read_long:
        movea.l 4(%sp),%a0
        move.l  (%a0),%d0
        rts

* short set_sr(short new_sr);
* set SR, return previous SR
* entry: arg = SR value
* exit:  d0 = previous SR value
        .global set_sr
set_sr:
        moveq   #0,%d0
        move.w  %sr,%d0
        move.l  4(%sp),%d1
        move.w  %d1,%sr
        rts

