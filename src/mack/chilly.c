#include "mack.h"

s8 memcmp(u8 *src, const u8 *from, u16 len)
{
	u16 i;
	for (i=0; i< len; i++)
	{
		if (*(src+i) > *(from+i))	return 1; 
		if (*(src+i) < *(from+i))	return -1;
	}

	return 0;
}
