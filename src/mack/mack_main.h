#ifndef MACKMAIN_H_
#define MACKMAIN_H_


#define SYSTEM_MODE0		0x00	//for bios boot only
#define	SYSTEM_MODE1_TYPE1	0x11	//boot on game cart w/o CD
#define SYSTEM_CART			SYSTEM_MODE1_TYPE1
#define SYSTEM_MODE1_TYPE2	0x12	//boot on game cart w/	CD
#define SYSTEM_CART_W_CD	SYSTEM_MODE1_TYPE2
#define SYSTEM_MODE2_TYPE1	0x21	//boot on CD w/o cart
#define SYSTEM_CD			SYSTEM_MODE2_TYPE1
#define SYSTEM_MODE2_TYPE2	0x22	//Boot on CD w/ cart
#define SYSTEM_CD_W_CART	SYSTEM_MODE2_TYPE2

#define BOOT_IN_PROGRESS	0
#define BOOT_DONE			1
#define	BOOT_ERROR			0xF


#define PLAY_MODE_TRACK	0x00
#define PLAY_MODE_ONCE	0xFF
#define PLAY_MODE_LOOP	0x01 // play track and repeat

//TODO do these status can be cumulative ?
//if yes, you must use '&' and not '=' to detect them
#define STATUS_DATA_READ	0x0000
#define	STATUS_DATA_READING	0x0001
#define STATUS_DATA_PAUSED	0x0003
#define STATUS_DATA_SEEK	0x0008

#define STATUS_STOPPED		0x0000
#define STATUS_PLAYING		0x0100
#define STATUS_SCANNING		0x0300
#define STATUS_PAUSED		0x0500
#define STATUS_SEEKING		0x0800

#define	STATUS_TOC_READ		0x0000
#define STATUS_NODISC		0x1000
#define STATUS_READING_TOC	0x2000
#define STATUS_OPEN			0x4000
#define STATUS_NOT_READY	0x8000

#define	STATUS_UNKNOWN		0xFFFF

#define	FLAG_DATA	0x04
#define	FLAG_EMP	0x02
#define	FLAG_MUTE	0x01

typedef struct
{
	u8 minutes;
	u8 seconds;
	u8 frame;
	u8 flags;
}disctime;


typedef struct { 
	u16 biosStatus;
	u16	led;
	struct 
	{
		u8 status;
		u8 report;
		u8 discControl;
		u8 songHexNumber;//in hexa (!!)
		disctime absoluteTime;
		disctime relativeTime;
		u8 firstTrackIdx;
		u8 lastTrackIdx;
		u8 driveVersion;
		u8 flags;
		disctime readOutTime;
	} cdd_status;
	u16 volume;
	disctime dataTime;
} discinfo;

//char wait_cmd_ack(void);
//void wait_do_cmd(char cmd);




u8 MackMain_boot( u8 sysMode );
u8 MackMain_isBooting( );

void MackMain_updateVBlank();

void MackMain_checkDisc();
void MackMain_CDDA_play(u8 track, u8 mode);
void MackMain_CDDA_pause();
#define MackMain_CDDA_resume	MackMain_CDDA_pause
void MackMain_CDDA_stop();
u8 MackMain_getTrackInfo(u8 track, u32 *trackTime, u8 *trackType);
u8 MackMain_getDiscInfo(discinfo *info);

#endif /* MACKMAIN_H_ */
